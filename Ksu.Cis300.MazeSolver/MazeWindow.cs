﻿/* MazeWindow.cs
 * Author: Matt Traudt
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ksu.Cis300.MazeLibrary;

namespace Ksu.Cis300.MazeSolver
{
    public partial class MazeWindow : Form
    {

        public MazeWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles when the button is pressed.
        /// Generates a new maze.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uxNewMazeButton_Click(object sender, EventArgs e)
        {
            uxMaze.Generate();
        }

        /// <summary>
        /// Handles when the maze is clicked on.
        /// Cleans up any previous drawn paths,
        /// calculates new path (if possible), and
        /// marks maze for redrawing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uxMaze_MouseClick(object sender, MouseEventArgs e)
        {
            Cell clickedCell = uxMaze.GetCellFromPixel(e.Location);
            if (uxMaze.IsInMaze(clickedCell))
            {
                uxMaze.EraseAllPaths();
                if (!recursiveDrawPathToExit(clickedCell, new bool[uxMaze.Height, uxMaze.Width]))
                    MessageBox.Show("Path not found.");
                else
                    uxMaze.Invalidate();
            }
        }

        /// <summary>
        /// This function will recursivly try to find an exit from 
        /// whatever starting cell is given. The idea is "tree like,"
        /// in that each level of recusion is a simplier version of the 
        /// same problem. This is why the provided cell is "start," because
        /// we could start from anywhere. An array of bools the size of the maze
        /// is also required in order to keep track of where we have been and to
        /// prevent walking in circles.
        /// </summary>
        /// <param name="start">Starting cell for this level of recursion</param>
        /// <param name="cells">array of bools to keep track of visited cells</param>
        /// <returns>true on successfully finding exit, otherwise false</returns>
        private bool recursiveDrawPathToExit(Cell start, bool[,] cells)
        {
            const bool VISITED = true; // for clarity
            cells[start.Row, start.Column] = VISITED;
            // 0     < 1    < 2     < 3
            // North < East < South < West
            for (Direction facingDirection = Direction.North; facingDirection <= Direction.West; facingDirection++)
            {
                // loop through all four directions, trying them in order
                if (!uxMaze.IsClear(start, facingDirection))
                {
                    // not clear
                    // don't do anything
                }
                else
                {
                    // clear in this direction. see what is ahead
                    Cell forwardCell = uxMaze.Step(start, facingDirection);
                    if (!uxMaze.IsInMaze(forwardCell))
                    {
                        // forward cell is not in maze. Found exit!
                        uxMaze.DrawPath(start, facingDirection);
                        return true;
                    }
                    else
                    {
                        // foward cell is still in maze
                        if (cells[forwardCell.Row, forwardCell.Column] != VISITED)
                        {
                            // foward cell is also unvisited, go there
                            
                            // go recurisve now. If "true" comes up the chain,
                            // then we found an exit, draw our part of the path,
                            // and keep returning true up the chain.
                            // if "false" comes up, then do nothing and continue
                            // trying to find a path
                            if (recursiveDrawPathToExit(forwardCell, cells))
                            {
                                uxMaze.DrawPath(start, facingDirection);
                                return true;
                            }
                        }
                        else
                        {
                            // been there
                            // don't do anything
                        }
                    }
                }
            }
            // we have exhausted all options
            // tell previous level that nothing good
            // can come from going this way.
            // if this is the top level of recursion, then
            // we know we can't get to an exit.
            return false;
        }
    }
}
